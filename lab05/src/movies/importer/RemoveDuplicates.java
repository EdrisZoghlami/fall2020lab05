//Edris Zoghlami
//1935687



package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {

	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir,false);
	}

	
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> noDouble = new ArrayList<String>();
		
		for(int i=0;i<input.size();i++) {
			
			if(noDouble.contains(input.get(i))) {
				
			}
			else {
				noDouble.add(input.get(i));
			}
		}
		
		return noDouble;
	}

}
