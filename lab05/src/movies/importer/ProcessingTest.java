//Edris Zoghlami
//1935687



package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		String source="C:\\Users\\edris\\fall2020lab05";
		String destination="C:\\Users\\edris\\fall2020lab05\\destination";
		LowercaseProcessor testOne =new LowercaseProcessor(source, destination);
		RemoveDuplicates testTwo =new RemoveDuplicates(destination,source);

		testOne.execute();
		
		testTwo.execute();
		
		
	}

}
